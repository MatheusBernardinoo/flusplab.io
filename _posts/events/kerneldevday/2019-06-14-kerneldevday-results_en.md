---
layout: post
categories: events
title: "KernelDevDay Results"
lang: en
ref: kddResultsPage2019
redirect_from: /events/2019/06/14/kerneldevday-results_en
author: marcelosc
excerpt_separator: <!--end-abstract-->
---

In which we give a brief overview of everything that happened on
KernelDevDay, giving a step-by-step account of the event and showing
some statistics on patches sent on that day.

<!--end-abstract-->

{% include add_multilang.html %}

### Achieved Goals

We are glad to announce that KernelDevDay (KDD) was a huge success, achieving
our goal of hosting the very first Linux Kernel contribution get-together at the
University of São Paulo (USP). By fostering this new community of FLOSS
developers, we were able encourage a new generation of contributors to dive into
the world of Linux Kernel development. During the event's closing, many
participants gave their feedback on the event, which we transcribe a few here:

>"The atmosphere was always very light and pleasant. The mentors [more
>experienced participants] were always willing to help; at no time did I feel
>intimidated to ask questions. Thank you very much for the initiative! I look
>forward to the next one :)"

>"You created a very comfortable atmosphere in this event, I was not afraid to
ask anything. It might be good to do an event with a wider range of
contribution options, but I understand it's tricky, since it's too much with
so little time. Anyway, the event was very good, thank you."

>"The event was excellent! For a "beginner" like me, it was possible to
experience for a day, the modus operandi of Linux kernel developers."

<!--
Depoimentos originais:
O ambiente do evento estava sempre muito leve e agradável. Os mentores estavam
sempre dispostos a ajudar; em nenhum momento me senti intimidado a fazer
perguntas. Muito obrigado pela iniciativa! Espero ansiosamente pelos próximos :)

Vocês criaram um ambiente muito confortável nesse evento, não tive nenhum receio
de perguntar nada. Talvez fosse bom fazer um evento com um range maior de opções
de contribuição, mas entendo que é complicado, já que é muito complexo pra tão
pouco tempo. Enfim, o evento foi muito bom, muito obrigado.

O evento foi excelente! Para um "iniciante" <trecho indecifrável> no linux,
como eu, foi possível vivenciar por um dia, o modus operandi dos desenvolvedores
do Kernel Linux. ^^
-->

### Accomplishments

As a direct result of our work on May 18th, sixteen patches were submitted to
the IIO mailing list; eight of which were applied and will be merged into Greg's
tree soon. With this, twelve developers had their names authored in the Linux
kernel, of which seven were newcomers. We also had great reviews from the IIO
community, meaning that our contributions were welcomed in the community.  Many
suggestions were given on how to improve our contributions, which means most
patches that haven't been applied yet may get a revision. We shall now present
some statistics on the KernelDevDay below.

* 21 developers arranged in 9 teams
* 15 patches submitted to IIO during KDD
* 16 patches submitted as a direct result of work at KDD
* 7 patches immediately accepted
* 21 replies from kernel community in the following two days
* 12 developers had their names committed to the Linux kernel
* 7 newcomers had their names committed to the Linux kernel
* 10 patches initially developed at KDD were accepted so far (2019-06-14)
* 10 newcomers were introduced to the Linux kernel at KDD

### What happened at KernelDevDay

KDD officially started on May 17th (Friday) with a warmup for newcomers,
introducing several elementary kernel tasks. We first introduced FLUSP as a
whole, followed by a brief overview on the Linux kernel and its subsystems, the
maintainers and community. The community review flow and an overview of the
kernel development workflow were also covered. We then helped attendees with
their development environment, which included: repository cloning, mailing list
subscription, kernel compilation, and git configuration.

This warmup was proceded by the actual event itself, KernelDevDay, on May 18th.
Participants were greeted at reception at eight o’clock. From 8:00 to 8:40
attendees received an identification badge, FLUSP t-shirts, stickers, and
keychains. We then had KDD's official opening start with a talk about our FLOSS
students group: FLUSP; our history, values, goals, and achievements. We also brought
some statistics about the Linux kernel, it's comprehensiveness and importance to
other great projects, giving attendees some motivation to contribute. As a final
note, we presented promotional videos from our sponsors, highlighting their
support with the Linux project.

{% include slideshow.html
    slideShowIndex = 0
    images = "subscription1.jpg,subscription3.jpg,flusp_keychains.jpg,flusp_stickers_n_keychains.jpg,flusp_stickers.jpg,flusp_toasts.jpg,flusp_intro1.jpg,flusp_intro3.jpg,flusp_intro4.jpg,flusp_intro5.jpg"
    caption_list="Preparations for KDD,Reception at KDD,FLUSP keychains,Stickers and keychains,FLUSP and Digital Ocean's stickers,FLUSP swag,FLUSP intro,FLUSP intro,Sponsor video,Sponsor video"
%}

Following the event intro, we presented an introduction to the IIO subsystem,
explaining its purpose, what sort of devices it supports, how IIO device drivers
interact with hardware, what are the correlated kernel interfaces, and the
fundamentals of driver structure and functions. We then finished our more
technical intro giving some guidelines on how to contribute to a driver. A
process that includes studying the driver code, reading the IIO documentation
and device datasheet, and looking at previous patches sent to the driver.

{% include add_image.html
   src="flusp_iiointro.jpg"
   caption="IIO intro"
%}

At around 9:30 AM, we divided the 21 developers into 9 teams to work on
previously selected issues. Our goal was to contribute to drivers in the IIO
staging area in an effort to make them more compliant with the new IIO API and,
hopefully, move them to the mainline. Since it was a single day event, we
focused on documentation and smaller contributions. After choosing issues to
work on, we set up a kanban to track development flow. The rest of the morning
was dedicated to working on the issues and updating the kanban accordingly.

{% include slideshow.html
    slideShowIndex = 1
    images="kanban3.jpg,kanban4.jpg,morning1.jpg,morning5.jpg,morning6.jpg"
    caption_list="Kanban setup,Kanban,Coding throughout the morning,Coding throughout the morning 2,Coding throughout the morning 3"
%}

At midday, we took a lunch break. We had bread, peanut candies, many flavors of
juice, and a carrot cake. The attendees used this moment for socialization; they
talked about technical things such as devicetree, other kernel subsystems, FLOSS
projects, etc. We also took a photo with those that consented with their photo
being taken.

{% include slideshow.html
    slideShowIndex = 2
    images="lunch1.jpg,lunch6.jpg,lunch9.jpg,lunch10.jpg,lunch12.jpg,kdd_folks.jpg"
    caption_list="Lunch served,Lunch time,Lunch time 2,Lunch time 3,All work and no play makes Jack a dull boy,Some folks at KernelDevDay"
%}

We kept on working throughout the afternoon, having a thirty-minute coffee break
at 3 PM. During both morning and afternoon, participants had at least two
available mentors/coaches helping them with their issues, anything related to
kernel, and patch mailing. We had an internal review on patches, asking
participants to submit their work first on the FLUSP mailing list. After 5 PM,
we intensified the review process to improve patch quality, increasing their
chances of being accepted when submitting to the IIO mailing list. We had up to
four coaches reviewing patches at a time! To our surprise, most participants
were still going strong as late as 7 PM, working overtime to get their patches
up to the Kernel standard. For this reason, we decided to extend the event and
finally close KDD at 8 PM, one hour past our original closing time.

{% include slideshow.html
    slideShowIndex = 3
    images="afternoon1.jpg,afternoon3.jpg,afternoon4.jpg"
    caption_list="Coding throughout the afternoon,Coding throughout the afternoon 2,Coding throughout the afternoon 3"
%}

We finally closed the event by going through each of the completed issues on the
Kanban, and acknowledging each patch sent out to the IIO mailing list.
Surprisingly, 19 out of 21 developers had their patches submitted to the kernel.
We encouraged participants to keep working on their patches after the kernel
community review. Overall, it was a fun "collaborathon"!

### Patches

Here is the list of all patches sent during the event.

| Patch Title | Status |
|-------------|:------:|
| [staging: iio: adis16240: add device to module device table] | APPLIED |
| [staging: iio: ad9834: add of_device_id table] | APPLIED |
| [staging:iio:ad7150: fix threshold mode config bit] | APPLIED |
| [staging: iio: ad7746: add device tree support] | APPLIED |
| [staging: iio: adt7316: create of_device_id array] | APPLIED |
| [staging: iio: cdc: ad7150: create macro for capacitance channels] |  APPLIED |
| [staging: iio: adis16203: Add of_device_id table] | APPLIED |
| [staging: iio: adis16240: add of_match_table entry] | APPLIED |
| [dt-bindings: iio: accel: adxl372: switch to YAML bindings] | APPLIED |
| [dt-bindings: iio: adc: add adi,ad7780.yaml binding] | APPLIED |
| [dt-bindings: iio: ad7949: switch binding to yaml] | Under review/reworking |
| [staging: iio: ad2s1210: Destroy mutex at remove] | Under review/reworking |
| [staging: iio: ad2s1210: Add devicetree yaml doc] | Under review/reworking |
| [staging: iio: ad9832: Add device tree support] | Under review/reworking |
| [staging: iio: ad7192: create of_device_id array] | Under review/reworking |
| [staging: iio: cdc: ad7150: create of_device_id array] | Under review/reworking |
| [dt-bindings: iio: ad5933: switch to YAML bindings] | Not submitted yet |

[staging: iio: adis16240: add device to module device table]: https://patchwork.kernel.org/patch/10949199/
[dt-bindings: iio: accel: adxl372: switch to YAML bindings]: https://patchwork.kernel.org/patch/10970713/
[staging: iio: ad9834: add of_device_id table]: https://patchwork.kernel.org/patch/10949225/
[dt-bindings: iio: ad7949: switch binding to yaml]: https://patchwork.kernel.org/patch/10949209/
[dt-bindings: iio: adc: add adi,ad7780.yaml binding]: https://patchwork.kernel.org/patch/10960619/
[staging:iio:ad7150: fix threshold mode config bit]: https://patchwork.kernel.org/patch/10949247/
[staging: iio: ad2s1210: Destroy mutex at remove]: https://patchwork.kernel.org/patch/10949203/
[staging: iio: ad2s1210: Add devicetree yaml doc]: https://patchwork.kernel.org/patch/10949205/
[staging: iio: ad9832: Add device tree support]: https://patchwork.kernel.org/patch/10949085/
[staging: iio: ad7746: add device tree support]: https://patchwork.kernel.org/patch/10949207/
[staging: iio: adt7316: create of_device_id array]: https://patchwork.kernel.org/patch/10949221/
[staging: iio: ad7192: create of_device_id array]: https://patchwork.kernel.org/patch/10949223/
[staging: iio: cdc: ad7150: create macro for capacitance channels]: https://patchwork.kernel.org/patch/10949217/
[staging: iio: cdc: ad7150: create of_device_id array]: https://patchwork.kernel.org/patch/10949193/
[staging: iio: adis16203: Add of_device_id table]: https://patchwork.kernel.org/patch/10949219/
[dt-bindings: iio: ad5933: switch to YAML bindings]: nolink
[staging: iio: adis16240: add of_match_table entry]: https://patchwork.kernel.org/patch/10959065/

### Slides

Here are the slides we used during our event opening.

<a href="{{ site.baseurl }}/assets/presentations/KDD-Warmup.pdf">KDD Warmup</a><br/>
<a href="{{ site.baseurl }}/assets/presentations/KDD-Intro.pdf">KDD Intro</a><br/>
<a href="{{ site.baseurl }}/assets/presentations/KDD-IIO-Intro.pdf">KDD IIO Intro</a><br/>

### Special thanks

We know that KernelDevDay wouldn't have been such a great experience for
everyone had we not had help from many people. We would like to give a special
thanks to the following people:

* All FLUSP members who helped with the event organization
* USPCodeLab
* Edina Arouca
* Nelson Lago
* Rodrigo Orem
* Alfredo Goldman vel Lejbman
* Alexandru Ardelean
* Michael Hennerich
* Jonathan Cameron
* Gustavo Padovan
* Our sponsors: Analog Devices, Collabora, Digital Ocean, ProFUSION

### Contact:

For more information, feel free to send us an e-mail:
[flusp@googlegroups.com](mailto:flusp@googlegroups.com)

### Sponsors:

<div class="text-center">
  <a href="https://www.digitalocean.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/DO.svg" width="251px">
  </a>
  <a href="https://www.analog.com/en/index.html/" class="spoonmsor_logo">
    <img src="/img/sponsors/ADI.png">
  </a>
  <a href="https://profusion.mobi/" class="spoonmsor_logo">
    <img src="/img/sponsors/ProFusion.png" width="251px">
  </a>
  <a href="https://www.collabora.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/Collabora.png" width="185px">
  </a>
</div>
