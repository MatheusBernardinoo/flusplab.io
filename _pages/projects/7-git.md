---
title:  "Git"
image:  "git-icon"
ref: "git"
categories: "projects.md"
id: 7
---

# Description

Version control system

# Recommendations

* Comfortable with C, pointers and bitwise operations
* Basic knowledge of shell script and Perl (if you wish to contribute to commands written in Perl)
* Basic knowledge of files in Unix (especially hidden files, symlinks and hard links)
* Basic knowledge of Makefile
* Comfortable with terminal and command line interface
* Knowledge on how to use Git is also very helpful

# URLs

* [Source code](https://git.kernel.org/pub/scm/git/git.git/)
* [Mailing list archive](https://public-inbox.org/git/)
* [Official website](https://git-scm.com)
