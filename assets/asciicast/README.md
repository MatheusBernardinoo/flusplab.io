# Asciicast support at FLUSP's website

Asciicast is a file format used by [Asciinema](https://asciinema.org/) to
record terminal sessions. FLUSP's website has support for asciicast playing
using the [asciinema-player](https://github.com/asciinema/asciinema-player)
project (which is bundled at `libs/asciinema-x.x.x/`).

There are three steps to add an asciinema-player instance to a page:

1. Copy the asciicast file to `assets/asciicast/<cast_filename>.cast`.
2. Define `asciicast: true` in the page's Front Matter.
3. Add the following where you want the player to be:
  `<div class="asciicast-target" data-fname="<cast_filename>"></div>`

Feel free to add other `class` or `style` properties to the `div`.
