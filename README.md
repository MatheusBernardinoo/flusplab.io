# FLUSP's website

## How to run locally

First, install the dependencies:

```
$ bundle install
```

Then:

```
$ bundle exec jekyll serve
```

## How to contribute

Contributions should be sent via merge request.

Feel free to create a new post or to contribute with other parts of the site.

## How to create a new post

Every post must have an identified author. Because of this, if you have never
done a post here before, please add yourself as an item in the `/_config.yml`
file:

```
# Authors
(...)
<author-nickname>:
  name: <Author Complete Name>
  web: <author.web.page.com>
```

### File name and Path

New posts must be written in markdown and sent as files on the `/_posts`
directory, following the the current directory tree pattern inside it. The post
file name must follow the pattern:
`YYYY-MM-DD-project-subproject-snake_cased_post_title.md`.

#### Examples

- If your nickname is `zubumafu` and you desire to create a blog post, this
  could be a valid path:
    - `/_posts/blogs/zubumafu/2023-11-03-bananas_are_cool.md`.

- And for an academic post:
    - `/_posts/academics/zubumafu/2023-11-03-why_bananas_are_cool.md`.

### File header

Each post file should contain the following header:

```
---
layout: post
title:  "project: subproject: Your Title Here"
date:   YYYY-MM-DD
categories: comma,separated,categories
author: <author-nickname>
---
```

Note that:

- `project` and `subproject` prefixes must only be present when the post refer
  to a specific project and/or subproject.
- `categories` must contain the page title where the post will appear. These
  are the possible ones:
    - `academics`
    - `blogs`
    - `gcc`
    - `git`
    - `kernel`
- `author` specifies the author nickname.

#### Examples

A blog post:
```
---
layout: post
title:  I've passed in Statistics!!
date:   2023-11-03
categories: blogs
author: zubumafu
---
```

An academic post:
```
---
layout: post
title:  I've passed in Statistics!!
date:   2023-11-03
categories: academics
author: zubumafu
---
```

A kernel post:
```
---
layout: post
title:  kernel: Playing with the iio-dummy
date:   2023-11-03
categories: kernel
author: zubumafu
---
```

A kernel IIO post:
```
---
layout: post
title:  kernel: iio: Playing with the iio-dummy
date:   2023-11-03
categories: kernel
author: zubumafu
---
```

As recommendation, look for other posts for further reference.

## Flossweek submodule

If you want this submodule when cloning:

```
$ git clone --recurse-submodules https://gitlab.com/flusp/site
```

If you want to update your local repository so that the submodule changes appear:

```
$ git pull --recurse-submodules
```

### Based on

- [Contrast](https://github.com/niklasbuschmann/contrast)
- [Jekyll](https://jekyllrb.com/)
- [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro)
- [Font Awesome](http://fontawesome.io/)
- [Pygments](https://github.com/richleland/pygments-css)
- [Pixyll](https://github.com/johnotander/pixyll)

## Add new projects

If you want to add (or update) a new project, you have to add a new file at
``_pages/projects/`` directory and following the pattern below:

```
touch _pages/projects/<ID>-<PROJECT_NAME>.md
```

Just take a look at the other project files in order to have some examples.
