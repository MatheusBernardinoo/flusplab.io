---
title: "About"
---

### About us

We are a group of graduate and undergraduate students at the University of
São Paulo (USP) that aims to contribute to free software (Free/Libre/Open
Source Software - FLOSS) projects. We have weekly meetings at the Centro
de Competência de Software Livre ([CCSL](http://ccsl.ime.usp.br/en)) at
the Institute of Mathematics and Statistics (IME-USP).

If you want to be a part of the group, simply join our social groups
through the links <a href="#contact">below</a>.

### Our values

- We value freedom of code and knowledge.
- We seek to be a flat organization.
- We believe sharing is paramount to our group. And we value the mutual
exchange of learning among members.
- We consider reviewing code a just as important task as adding code.
- We understand that every member should be ready to "hand over the
baton", sharing their knowledge about code and FLUSP as a whole to their
peers.

### Our organization

- **Reviewing code**: We believe that peer reviewing code improves its content.
Therefore, all contributions should be reviewed by another contributor.  If a
project is found to be constantly in wait for reviews, a discussion on "handing
over the baton" should be held by all members of the project. (See "do-ocracy"
below).

- **Mentoring system**: Mentors are the contributors with the most
experience in a project. They are meant to help reduce the learning
curve of newcomers, and are expected to, in time, "hand over the baton"
to another experienced contributor.

- **Decentralized**: The group is composed of subgroups, each working on
a specific free software project. The organization and meetings of each
subgroup are decided among its members.

- **Dynamicity**: We believe in a more dynamic organization, where
responsibilities and knowledge are passed to newer members, following
our value of "handing over the baton" to someone else.

- **Do-ocracy**: Responsibilities and tasks are assigned to the people
who do actual work, not elected or selected officials. We encourage
initiative, meaning that if you think you are capable of helping or
contributing to a task, go right ahead! We'll have your back.

### How to reach us
<div id="contact"></div>

You are welcome to join us at our:

1. FLUSP _Mailing list_: [https://groups.google.com/d/forum/flusp ](https://groups.google.com/d/forum/flusp)
2. GCC _Mailing list_: [https://groups.google.com/d/forum/gcc-flusp](https://groups.google.com/d/forum/gcc-flusp)
3. Linux Kernel _Mailing list_: [https://groups.google.com/d/forum/kernel-usp ](https://groups.google.com/d/forum/kernel-usp)
4. IRC _Server_: **irc.freenode.net**. Channel: **#ccsl-usp**
5. Telegram _Chat group_: [join here](https://t.me/FLUSP2).
6. YouTube _Channel_: [https://www.youtube.com/channel/UCYkMGyGL8h9vz06PcEjivWw](https://www.youtube.com/channel/UCYkMGyGL8h9vz06PcEjivWw)
7. Facebook _Page_: [https://www.facebook.com/flusp/](https://www.facebook.com/flusp/)

### How can an external organization help us?

Any help is immensely appreciated. Here are some ways an
external organization (or person) could help us.

- Lectures
- Hardware donation
- Grants
- Undergraduate capstone project (advisor)
- Research project (advisor)
- Internships and full-time jobs
